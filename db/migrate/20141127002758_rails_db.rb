class RailsDb < ActiveRecord::Migration
  def change
    add_column :users, :lastName, :string
    add_column :users, :userName, :string
    add_column :users, :phoneNumber, :string
  end
end
